import React, { useState } from 'react';
import './colorBox.scss';

function getRandomColor() {
    const LIST_COLOR = ['deeppink', 'green', 'red', 'purple', 'black'];
    const randomIndex = Math.trunc(Math.random() *5);
    return LIST_COLOR[randomIndex];
}

function ColorBox(props) {
    const [color, setColor] = useState(() => {
        const initColor = localStorage.getItem('box-color') || 'deeppink' ;
        return initColor;
    });

    function handleBoxClick() {
        const newColor = getRandomColor();
        setColor(newColor);
        localStorage.setItem('box-color', newColor);
    }

    return (
        <div className="color-box" onClick={handleBoxClick} style={{ backgroundColor: color }}>
            COLOR BOX
        </div>
    );
}

export default ColorBox;