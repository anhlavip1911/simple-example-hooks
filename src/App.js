import React, { useEffect, useState } from 'react';
import queryString from 'query-string'
import './App.scss'
import ColorBox from './components/colorBox';
import Pagination from './components/Pagination';
import PostList from './components/PostList';
import TodoForm from './components/TodoForm';
import TodoList from './components/TodoList';

function App() {
    const [todoList, setTodoList] = useState([
        { id: 1, title: 'I love Easy Frontend! 😍 ' },
        { id: 2, title: 'We love Easy Frontend! 🥰 ' },
        { id: 3, title: 'They love Easy Frontend! 🚀 ' },
    ]);

    const [postList, setPostList] = useState([]);
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRow: 1,
    });

    const [filters, setFilters] = useState({
        _limit: 10,
        page: 1,
    });

    useEffect(() => {
        async function fetchPostList() {
            try {
                const paramsString = queryString.stringify(filters);
                const requestUrl = `http://js-post-api.herokuapp.com/api/posts?${paramsString}`;
                const response = await fetch(requestUrl);
                const responseJson = await response.json();
                console.log({ responseJson });

                const { data, pagination } = responseJson;
                setPostList(data);
                setPagination(pagination);
            } catch (error) {
                console.log('Error Message : ', error.message);
            }
        }

        fetchPostList();
    }, [filters])

    function handlePageChange(newPage) {
        console.log("New page : " + newPage);
        setFilters({
            ...filters,
            _page: newPage
        });
    }

    function onTodoClick(todo) {
        console.log(todo);
        const index = todoList.findIndex(x => x.id === todo.id);
        if (index < 0) return;

        const newTodoList = [...todoList];
        newTodoList.splice(index, 1);
        setTodoList(newTodoList);
    }

    function handleTodoFormSubmit(formValues) {
        const newTodo = {
            id: todoList.length + 1,
            ...formValues
        };
        const newTodoList = [...todoList];
        newTodoList.push(newTodo);
        setTodoList(newTodoList);
        console.log(formValues);
    }

    return (
        <div className="app">
            <h1>React Hooks: Todo List</h1>
            <PostList posts={postList} />
            <Pagination pagination = {pagination} onPageChange = {handlePageChange} />
            {/* <TodoForm onSubmit={handleTodoFormSubmit}/>
            <TodoList onTodoClick={onTodoClick} todos={todoList} /> */}
        </div>
    );
}

export default App;
